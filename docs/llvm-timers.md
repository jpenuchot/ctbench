# LLVM Timers

Quick summary of what Clang/LLVM's timers measure.

## ExecuteCompiler

Complete compiler execution time.

## Frontend

> TODO

## Source

The Source timer measures pre-processing time. It can be used to ensure that
benchmark generation using macros has a negligible impact on final benchmark
results.

## ParseClass

> TODO

## InstantiateClass

> TODO

## Backend

> TODO

## ParseTemplate

> TODO

## OptModule

> TODO

## CodeGenPasses

> TODO

## PerModulePasses

> TODO

## PerFunctionPasses

> TODO

## PerformPendingInstantiations

> TODO
