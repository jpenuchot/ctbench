# grapher-utils

`grapher-utils` is an executable that provides plotter-specific commands.

It should be used to get default configs for the plotters, as well as basic
plotter information.
