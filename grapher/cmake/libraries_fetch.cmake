# ==============================================================================
# nlohmann::json

set(JSON_BuildTests OFF)
set(JSON_Install OFF)
set(JSON_MultipleHeaders ON)
set(JSON_ImplicitConversions ON)
set(JSON_Diagnostics ON)
set(JSON_CI OFF)

FetchContent_Declare(
  json_content
  GIT_REPOSITORY https://github.com/nlohmann/json.git
  GIT_TAG v3.9.1
  GIT_SHALLOW)

FetchContent_MakeAvailable(json_content)

target_link_libraries(grapher PUBLIC nlohmann_json::nlohmann_json)

# ==============================================================================
# Sciplot

FetchContent_Declare(
  sciplot_content
  GIT_REPOSITORY https://github.com/sciplot/sciplot.git
  GIT_TAG master
  GIT_SHALLOW)

FetchContent_GetProperties(sciplot_content)
if(NOT sciplot_content_POPULATED)
  FetchContent_Populate(sciplot_content)
endif()

target_include_directories(grapher PUBLIC ${sciplot_content_SOURCE_DIR})

# ==============================================================================
# TTS

if(CTBENCH_ENABLE_TESTING)
  FetchContent_Declare(
    tts_content
    GIT_REPOSITORY https://github.com/jfalcou/tts.git
    GIT_TAG 1.0
    GIT_SHALLOW)

  FetchContent_GetProperties(tts_content)
  if(NOT tts_content_POPULATED)
    FetchContent_Populate(tts_content)
  endif()

  target_include_directories(grapher PUBLIC ${tts_content_SOURCE_DIR}/include)
endif()
